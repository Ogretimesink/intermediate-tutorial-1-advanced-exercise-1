/*
-----------------------------------------------------------------------------
Filename:	TutorialApplication.h
-----------------------------------------------------------------------------
*/

#ifndef __TutorialApplication_h_
#define __TutorialApplication_h_

#include <OgreRoot.h>
#include <OgreCamera.h>
#include <OgreViewport.h>
#include <OgreSceneManager.h>
#include <OgreRenderWindow.h>
#include <OgreConfigFile.h>
#include <OgreException.h>
#include <OgreEntity.h>
#include <OgreFrameListener.h>
#include <OgreWindowEventUtilities.h>
#include <Terrain/OgreTerrain.h>
#include <Terrain/OgreTerrainGroup.h>
 
#include <OISEvents.h>
#include <OISInputManager.h>
#include <OISKeyboard.h>
#include <OISMouse.h>
 
#include <SdkCameraMan.h>
 
class TutorialApplication
	: public Ogre::WindowEventListener
	, public Ogre::FrameListener
	, public OIS::KeyListener
	, public OIS::MouseListener
{
public:
	// C++ application constructor
	TutorialApplication(void);

	// C++ application destructor
	virtual ~TutorialApplication(void);
 
	// Start the application
	virtual void go(void);

private:
	// Function to update scene every frame
	virtual bool frameRenderingQueued(const Ogre::FrameEvent& fe);
 
	// Function to proces keyboard input
	virtual bool keyPressed(const OIS::KeyEvent& ke);

	// Function to proces keyboard input
	virtual bool keyReleased(const OIS::KeyEvent& ke);
 
	// Function to proces mouse input
	virtual bool mouseMoved(const OIS::MouseEvent& me);

	// Function to proces mouse input
	virtual bool mousePressed(const OIS::MouseEvent& me, OIS::MouseButtonID id);

	// Function to proces mouse input
	virtual bool mouseReleased(const OIS::MouseEvent& me, OIS::MouseButtonID id);
 
	// Adjust mouse clipping area
	virtual void windowResized(Ogre::RenderWindow* rw);

	// Unattach OIS before window shutdown
	virtual void windowClosed(Ogre::RenderWindow* rw);
 
	// Function to set up individual parts of the application
	bool setup();

	// Function to show the configuration dialog box
	bool configure();

	// Function to initialize the scene manager object
	void chooseSceneManager();

	// Function to initialize the scene camera
	void createCamera();

	// Function to fill the scene with graphic objects
	void createScene();

	// Function to clean up objects before shutdown
	void destroyScene();

	// Function to initialize input objects
	void createFrameListener();

	// Function to initialize the scene viewports
	void createViewports();

	// Function to load resources from configuration files
	void setupResources();

	// Function to create any resource listeners for loading screens
	void createResourceListener();

	// Function to initialize all of the resources found by the application
	void loadResources();
 
	// Flag to shutdown the application
	bool mShutdown;
 
	// Root object instance for the application
	Ogre::Root* mRoot;

	// Camera object for viewing the scene
	Ogre::Camera* mCamera;

	// Scene manager object that controls all things rendered on the screen
	Ogre::SceneManager* mSceneMgr;

	// Window object instance for rendering the application
	Ogre::RenderWindow* mWindow;

	// Resource configuration file name
	Ogre::String mResourcesCfg;

	// Plugin configuration file name
	Ogre::String mPluginsCfg;
 
	// Basic camera controller object
	OgreBites::SdkCameraMan* mCameraMan;
 
	// OIS Input device manager object
	OIS::InputManager* mInputMgr;

	// Mouse input device object
	OIS::Mouse* mMouse;

	// Keyboard input device object
	OIS::Keyboard* mKeyboard;
 
	// Class for robots
	class Robot
	{
		public:
			// Deque of vector locations that are used as robot destinations
			std::deque<Ogre::Vector3> mWalkList;

			// Distance between the robot location and destination
			Ogre::Real mDistance;

			// Robot walking speed
			Ogre::Real mWalkSpd;

			// Robot in motion flag
			bool mWalking;

			// Robot walking direction
			Ogre::Vector3 mDirection;

			// Robot walking destination
			Ogre::Vector3 mDestination;

			// Current robot animation
			Ogre::AnimationState* mAnimationState;

			// Computer graphic model instance
			Ogre::Entity* mEntity;

			// Scene object
			Ogre::SceneNode* mNode;
	};

	// Map data structure used to store individual robot data
	std::map <int, Robot> RobotList;

	// Total number of robots in the scene
	int RobotTotal;

	// Retrieve the next walking destination for the robot
	bool nextLocation(std::map<int, Robot>::iterator RobotIterator);

	// Add robot destination locations
	void addLocations(std::map<int, Robot>::iterator RobotIterator);
 
};

//---------------------------------------------------------------------------

#endif // #ifndef __TutorialApplication_h_

//---------------------------------------------------------------------------